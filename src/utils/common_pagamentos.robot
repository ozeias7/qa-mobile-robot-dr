*** Settings ***
Documentation           Todas as keywords que poderão ser utilizadas de forma híbrida estarão presentes aqui.

Resource                ../config/package.robot

*** Keywords ***
Pagamento cartao de credito adyen 1x
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_NUMERO_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_CARTAO}            ${ADYEN.NUMBER}
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_VALIDADE_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_MES_CARTAO}        ${ADYEN.MONTH}
    Input Text                              ${PAGAMENTO_CC.INPUT_ANO_CARTAO}        ${ADYEN.YEAR}
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_CVV_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_CVV_CARTAO}        ${ADYEN.CVV}
    Click Element                           ${PAGAMENTO_CC.LABLE_CVV_CARTAO}        
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_NOME_IMPRESSO_CARTAO}  
    Click Element                           ${PAGAMENTO_CC.INPUT_NOME_CARTAO}               
    Input Text                              ${PAGAMENTO_CC.INPUT_NOME_CARTAO}       ${ADYEN.NAME}
    Click Element                           ${PAGAMENTO_CC.LABLE_NOME_IMPRESSO_CARTAO}   
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_PARCELA_CARTAO}  
    Click Element                           ${PAGAMENTO_CC.RADIO_BUTTON_1X} 

Pagamento cartao de credito adyen 2x
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_NUMERO_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_CARTAO}            ${ADYEN.NUMBER}
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_VALIDADE_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_MES_CARTAO}        ${ADYEN.MONTH}
    Input Text                              ${PAGAMENTO_CC.INPUT_ANO_CARTAO}        ${ADYEN.YEAR}
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_CVV_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_CVV_CARTAO}        ${ADYEN.CVV}
    Click Element                           ${PAGAMENTO_CC.LABLE_CVV_CARTAO}        
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_NOME_IMPRESSO_CARTAO}  
    Click Element                           ${PAGAMENTO_CC.INPUT_NOME_CARTAO}               
    Input Text                              ${PAGAMENTO_CC.INPUT_NOME_CARTAO}       ${ADYEN.NAME}
    Click Element                           ${PAGAMENTO_CC.LABLE_NOME_IMPRESSO_CARTAO}   
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_PARCELA_CARTAO}  
    Click Element                           ${PAGAMENTO_CC.RADIO_BUTTON_2X}                                  

Pagamento cartao de credito adyen 3x
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_NUMERO_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_CARTAO}            ${ADYEN.NUMBER}
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_VALIDADE_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_MES_CARTAO}        ${ADYEN.MONTH}
    Input Text                              ${PAGAMENTO_CC.INPUT_ANO_CARTAO}        ${ADYEN.YEAR}
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_CVV_CARTAO}                
    Input Text                              ${PAGAMENTO_CC.INPUT_CVV_CARTAO}        ${ADYEN.CVV}
    Click Element                           ${PAGAMENTO_CC.LABLE_CVV_CARTAO}        
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_NOME_IMPRESSO_CARTAO}  
    Click Element                           ${PAGAMENTO_CC.INPUT_NOME_CARTAO}               
    Input Text                              ${PAGAMENTO_CC.INPUT_NOME_CARTAO}       ${ADYEN.NAME}
    Click Element                           ${PAGAMENTO_CC.LABLE_NOME_IMPRESSO_CARTAO}   
    Wait Until Element Is Visible           ${PAGAMENTO_CC.LABLE_PARCELA_CARTAO}  
    Click Element                           ${PAGAMENTO_CC.RADIO_BUTTON_3X} 