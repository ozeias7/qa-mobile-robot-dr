*** Settings ***
Documentation           Arquivo responsável por armazenar todas as variáveis e identificadores dos componentes da tela Home
...                     do aplicativo do banco.

*** Variables ***
&{PAGAMENTO_CC}
...         LABLE_NUMERO_CARTAO=//*[contains(@text, 'Qual é o número do cartão ?')]
...         INPUT_CARTAO=//*[contains(@text, 'Número do cartão')]
...         LABLE_VALIDADE_CARTAO=//*[contains(@text, 'Qual é a validade ?')]
...         INPUT_MES_CARTAO=//*[contains(@text, 'MM')]
...         INPUT_ANO_CARTAO=//*[contains(@text, 'AA')]
...         LABLE_CVV_CARTAO=//*[contains(@text, 'Qual o código de segurança (CVV) ?')]
...         INPUT_CVV_CARTAO=//*[contains(@text, 'Ex: 123')]
...         LABLE_NOME_IMPRESSO_CARTAO=//*[contains(@text, 'Qual é o nome que está impresso no cartão ?')]
...         INPUT_NOME_CARTAO=//*[contains(@text, 'Ex: Roberto Carlos')]
...         LABLE_PARCELA_CARTAO=//*[contains(@text, 'Quer parcelar em quantas vezes')]
...         RADIO_BUTTON_1X=//*[contains(@text, '1x de R$90.00')]
...         RADIO_BUTTON_2X=//*[contains(@text, '2x de R$45.00')]
...         RADIO_BUTTON_3X=//*[contains(@text, '3x de R$30.00')]
...         LABLE_TOTAL=//*[contains(@text, 'Total')]
...         BTN_REALIZAR_PAGAMENTO=//*[contains(@text, 'Realizar pagamento')]
