*** Settings ***
Documentation           Arquivo responsável por armazenar todas as variáveis e identificadores dos componentes da tela de login

*** Variables ***
&{LOGIN}

...             CAMP_EMAIL_TELEFONE=//*[contains(@text, 'Preencha aqui')]
...             CAMP_EMAIL_CADASTRO=//*[contains(@text, 'E-mail')]
...             CAMP_CADASTRO_CEL=//*[contains(@text, '(99) 999999999')]
...             CAMP_CADASTRO_CPF=//android.widget.EditText[contains(@text,'CPF')]       ##//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[8]/android.widget.EditText
...             CAMP_CADASTRO_CEP=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
...             CAMP_CADASTRO_NUM=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[9]/android.widget.EditText
...             CAMP_CADASTRO_COMPLEMENTO=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[2]
...             CAMP_CADASTRO_NOME=//*[contains(@text, 'Nome')]
...             CAMP_CADASTRO_DIA_NASC=//*[contains(@text, 'DD')]
...             CAMP_CADASTRO_MES_NASC=//*[contains(@text, 'MM')]
...             CAMP_CADASTRO_ANO_NASC=//*[contains(@text, 'AAAA')]
...             CAMP_CADASTRO_SENHA=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[12]/android.widget.EditText

...             BTN_PROXIMO=//*[contains(@text, 'Próximo')]
...             BTN_CONTINUAR=//*[contains(@text, 'Continuar')]
...             BTN_ENTRAR=//android.widget.TextView[contains(@text,'Entrar')]
...             BTN_CADASTRAR=//*[contains(@text, 'Cadastrar')]
...             BTN_ACEITO=//*[contains(@text, 'Aceito')]

...             LABEL_SEXO=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[10]/android.widget.ImageView
...             LABEL_VALIDATION=Olá Mobile,
...             LABEL_VALIDATION_VISITANTE=Olá visitante,
...             LABEL_VALIDATION_EMAIL=O e-mail informado é inválido.
...             LABEL_VALIDATION_PHONE=Informe seu celular com DDD por exemplo 11999999999
...             LABEL_VALIDATION_PASSWORD=Parece que esta não é a senha correta
...             LABEL_VALIDATION_NULL=Informe um celular ou e-mail para continuar
...             LABEL_VALIDATION_CADASTRO=Cadastro com sucesso

...             LINK_NAVEGAR_SEM_CONTA=//*[contains(@text, 'ou, navegue sem uma conta')]

...             INPUT_SENHA=//*[contains(@text, 'Preencha aqui')]