*** Settings ***
Documentation           Todas as keywords que poderão ser utilizadas de forma híbrida estarão presentes aqui.

Resource                ../config/package.robot

*** Keywords ***
Formulario de dados
    [Documentation]    Keyword para criar dados fakes, podendo inserir novas variveis, caso necessário.
    ...                para utilizar a keyword basta chamar ela na keyword que esta criando e, com isso,
    ...                terá acesso a todas as variáveis existentes aqui, por meio do set variable.


  ${NOME_USUARIO}                 FakerLibrary.UserName 
  ${NOME_USUARIO}                 Remove String                   ${NOME_USUARIO}     ~   Ç   ´   ^   `   -   .
  Set Suite Variable              ${NOME_USUARIO}

  ${EMAIL_USUARIO}                Remove String                   ${NOME_USUARIO}@gmail.com      ${SPACE}    
  Set Suite Variable              ${EMAIL_USUARIO}

  ${CAMPO_CELULAR}                FakerLibrary.RandomNumber       digits=11
  Set Suite Variable              ${CAMPO_CELULAR}

  ${CPF_USUARIO}                  FakerLibrary.cpf
  Set Suite Variable              ${CPF_USUARIO}

