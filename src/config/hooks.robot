*** Settings ***
Documentation           Os processos de setUp e tearDown estarão presentes aqui neste arquivo.
...                     A ideia é termos segregado a inicialização de cada tecnologia.

Resource                package.robot

*** Keywords ***
Open Session Android Local
    Set Appium Timeout          ${CONFIG.MOBILE.DEFAULT.APPIUM_TIMEOUT}
    Open Application            http://localhost:4723/wd/hub
    ...                         automationName=UiAutomator2
    ...                         platformName=${CONFIG.MOBILE.ANDROID.PLATFORM_NAME}
    ...                         deviceName=${CONFIG.MOBILE.ANDROID.DEVICE_NAME}
    ...                         app=${CONFIG.MOBILE.ANDROID.APP_PATH}
    ...                         autoGrantPermissions=true


Close Session
    Capture Page Screenshot
    Close Application
