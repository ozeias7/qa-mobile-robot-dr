*** Settings ***
Documentation           Arquivo responsável por armazenar todas as variáveis e identificadores dos componentes da tela Home
...                     do aplicativo do banco.

*** Variables ***
&{HOME}
...         BTN_RECUSAR=RECUSAR
...         BTN_ENTRAR_HOME=//*[contains(@text, 'Entrar')]
...         BTN_POR_VIDEO=//*[contains(@text, 'Por Vídeo')]
...         BTN_CONTINUAR=//*[contains(@text, 'Continuar')]
...         BTN_CONSULTAS=//*[contains(@text, 'CONSULTAS')]
...         BTN_RADIO=xpath=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView
...         BTN_SELECIONAR=//*[contains(@text, 'Selecionar')]
...         BTN_COMO_SER_ATENDIDO=xpath=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]
...         BTN_SERVICOS=xpath=//android.widget.Button[@content-desc="Serviços, tab, 1 of 5"]/android.view.ViewGroup/android.widget.ImageView



...         IMG_LUPA=xpath=//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.ImageView
...         FIELD_BUSCA=class=android.widget.EditText

...         BANNER_TELEMED_2_0=//*[contains(@text, 'Consulte-se online com um clínico geral agora!')]
...         BANNER_NOVIDADE=//*[contains(@text, 'NOVIDADE')]