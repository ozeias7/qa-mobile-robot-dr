*** Settings ***
Documentation           Todas os BDD's convertidos em keywords de telemed 2.0 estarão presentes neste arquivo.

Resource                ../../config/package.robot

*** Keywords ***
Então devo realizar a consulta telemed 2.0 pagando em 1x
    Wait Until Element Is Visible           ${HOME.BANNER_TELEMED_2_0}
    Click Element                           ${HOME.BANNER_TELEMED_2_0}
    Wait Until Element Is Visible           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
    Click Element                           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
    Pagamento cartao de credito adyen 1x 
    Click Element                           ${PAGAMENTO_CC.BTN_REALIZAR_PAGAMENTO}
    sleep                                   3
    Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA}
    sleep                                   3
    Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA_MODAL}

Então devo realizar a consulta telemed 2.0 pagando em 2x
    Wait Until Element Is Visible           ${HOME.BANNER_TELEMED_2_0}
    Click Element                           ${HOME.BANNER_TELEMED_2_0}
    Wait Until Element Is Visible           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
    Click Element                           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
    Pagamento cartao de credito adyen 2x 
    Click Element                           ${PAGAMENTO_CC.BTN_REALIZAR_PAGAMENTO}
    sleep                                   3
    Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA}
    sleep                                   3
    Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA_MODAL}

Então devo realizar a consulta telemed 2.0 pagando em 3x
    Wait Until Element Is Visible           ${HOME.BANNER_TELEMED_2_0}
    Click Element                           ${HOME.BANNER_TELEMED_2_0}
    Wait Until Element Is Visible           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
    Click Element                           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
    Pagamento cartao de credito adyen 3x 
    Click Element                           ${PAGAMENTO_CC.BTN_REALIZAR_PAGAMENTO}
    sleep                                   3
    Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA}
    sleep                                   3
    Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA_MODAL}

#############################################################################
Então devo realizar a consulta telemed 2.0 pela pagina inicio
     Wait Until Element Is Visible           ${HOME.BANNER_NOVIDADE}
     Wait Until Element Is Visible           ${HOME.BTN_POR_VIDEO}
     Click Element                           ${HOME.BTN_POR_VIDEO}
     Wait Until Element Is Visible           ${HOME.BTN_CONTINUAR}
     Click Element                           ${HOME.BTN_CONTINUAR}
     Wait Until Element Is Visible           ${HOME.BTN_CONSULTAS}
     Click Element                           ${HOME.BTN_CONSULTAS}
     sleep      3
     Wait Until Element Is Visible           ${HOME.IMG_LUPA}
     Click Element                           ${HOME.IMG_LUPA}
     Wait Until Element Is Visible           ${HOME.FIELD_BUSCA}
     Input Text                              ${HOME.FIELD_BUSCA}    ${PROD.PROD_ONLINE}
     Click Element                           ${HOME.BTN_RADIO}
     Click Element                           ${HOME.BTN_RADIO}
     Click Element                           ${HOME.BTN_SELECIONAR}
     sleep      3
     Click Element                           ${HOME.BTN_COMO_SER_ATENDIDO}
     Wait Until Element Is Visible           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
     Click Element                           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
     Pagamento cartao de credito adyen 1x 
     Click Element                           ${PAGAMENTO_CC.BTN_REALIZAR_PAGAMENTO}
     sleep                                   3
     Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA}
     sleep                                   3
     Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA_MODAL}
#############################################################################
Então devo realizar a consulta telemed 2.0 pela pagina servicos
     Wait Until Element Is Visible           ${HOME.BTN_SERVICOS}
     Click Element                           ${HOME.BTN_SERVICOS}
     Wait Until Element Is Visible           ${HOME.BANNER_NOVIDADE}
     sleep      3
     Wait Until Element Is Visible           ${HOME.BTN_POR_VIDEO}
     Click Element                           ${HOME.BTN_POR_VIDEO}
     Wait Until Element Is Visible           ${HOME.BTN_CONTINUAR}
     Click Element                           ${HOME.BTN_CONTINUAR}
     Wait Until Element Is Visible           ${HOME.BTN_CONSULTAS}
     Click Element                           ${HOME.BTN_CONSULTAS}
     sleep      3
     Wait Until Element Is Visible           ${HOME.IMG_LUPA}
     Click Element                           ${HOME.IMG_LUPA}
     Wait Until Element Is Visible           ${HOME.FIELD_BUSCA}
     Input Text                              ${HOME.FIELD_BUSCA}    ${PROD.PROD_ONLINE}
     
     Click Element                           ${HOME.BTN_RADIO}
     Click Element                           ${HOME.BTN_RADIO}
     Click Element                           ${HOME.BTN_SELECIONAR}
     sleep      3
     Click Element                           ${HOME.BTN_COMO_SER_ATENDIDO}
     Wait Until Element Is Visible           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
     Click Element                           ${TELEMED_2_0.BTN_PAGAR_CONSULTA}
     Pagamento cartao de credito adyen 1x 
     Click Element                           ${PAGAMENTO_CC.BTN_REALIZAR_PAGAMENTO}
     sleep                                   3
     Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA}
     sleep                                   3
     Click Element                           ${TELEMED_2_0.BTN_SAIR_FILA_MODAL}