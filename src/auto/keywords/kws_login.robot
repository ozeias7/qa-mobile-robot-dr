*** Settings ***
Documentation           Todas os BDD's convertidos em keywords de login estarão presentes neste arquivo.

Resource                ../../config/package.robot

*** Keywords ***
Dado que eu entre no aplicativo
    Wait Until Element Is Visible           ${HOME.BTN_ENTRAR_HOME}
    Click Element                           ${HOME.BTN_ENTRAR_HOME}

Dado que eu abra o aplicativo
    Wait Until Element Is Visible           ${HOME.BTN_ENTRAR_HOME}
   

Quando inserir as credenciais de acesso de email
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.EMAIL}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}    
    Wait Until Element Is Visible           ${LOGIN.INPUT_SENHA}         
    Input Text                              ${LOGIN.INPUT_SENHA}    ${AUTH.PASSWORD}
    Wait Until Element Is Visible           ${LOGIN.BTN_ENTRAR}
    Click Element                           ${LOGIN.BTN_ENTRAR}

Quando inserir as credenciais de acesso de telefone
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.PHONE}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}    
    Wait Until Element Is Visible           ${LOGIN.INPUT_SENHA}         
    Input Text                              ${LOGIN.INPUT_SENHA}    ${AUTH.PASSWORD}
    Wait Until Element Is Visible           ${LOGIN.BTN_ENTRAR}
    Click Element                           ${LOGIN.BTN_ENTRAR}

Quando inserir as credenciais de email invalido
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.WRONG_EMAIL}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}

Quando inserir as credenciais de telefone invalido
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.WRONG_PHONE}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}

Quando inserir as credenciais de senha invalida
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.EMAIL}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}    
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.WRONG_PASSWORD}
    Wait Until Element Is Visible           ${LOGIN.BTN_ENTRAR}
    Click Element                           ${LOGIN.BTN_ENTRAR}

Quando não inserimos credenciais de email ou telefone
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE} 
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}

Quando escolhemos navegar sem uma conta
    Wait Until Element Is Visible           ${LOGIN.LINK_NAVEGAR_SEM_CONTA}
    Click Element                           ${LOGIN.LINK_NAVEGAR_SEM_CONTA}
########################################################################################################
Quando insiro dados novos para cadastro com email
    Formulario de dados
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}     ${EMAIL_USUARIO}

    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_CEL}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_CEL}       ${CAMPO_CELULAR}

    Wait Until Element Is Visible           ${LOGIN.BTN_PROXIMO}
    Click Element                           ${LOGIN.BTN_PROXIMO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_CPF}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_CPF}       ${CPF_USUARIO}

    Wait Until Element Is Visible           ${LOGIN.BTN_PROXIMO}
    Click Element                           ${LOGIN.BTN_PROXIMO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_NOME}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_NOME}      ${NOME_USUARIO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_DIA_NASC}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_DIA_NASC}       ${AUTH.DIA}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_MES_NASC}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_MES_NASC}       ${AUTH.MES}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_ANO_NASC}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_ANO_NASC}       ${AUTH.ANO}

    Wait Until Element Is Visible           ${LOGIN.LABEL_SEXO}
    Click Element                           ${LOGIN.LABEL_SEXO}
    Click Element                           ${LOGIN.LABEL_SEXO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_SENHA}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_SENHA}       ${AUTH.PASSWORD}

    Wait Until Element Is Visible           ${LOGIN.BTN_PROXIMO}
    Click Element                           ${LOGIN.BTN_PROXIMO}
    
    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_CEP}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_CEP}        ${AUTH.CEP}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_NUM}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_NUM}                ${AUTH.NUM}
    
    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_COMPLEMENTO}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_COMPLEMENTO}        ${AUTH.COMPLEMENTO}

    Wait Until Element Is Visible           ${LOGIN.BTN_CADASTRAR}
    Click Element                           ${LOGIN.BTN_CADASTRAR}

    Wait Until Element Is Visible           ${LOGIN.BTN_ACEITO}
    Click Element                           ${LOGIN.BTN_ACEITO}
########################################################################################################
Quando insiro dados novos para cadastro com celular
    Formulario de dados
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}     ${CAMPO_CELULAR}

    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}

    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_CADASTRO}                
    Input Text                              ${LOGIN.CAMP_EMAIL_CADASTRO}          ${EMAIL_USUARIO}

    Wait Until Element Is Visible           ${LOGIN.BTN_PROXIMO}
    Click Element                           ${LOGIN.BTN_PROXIMO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_CPF}                 
    Input Text                              ${LOGIN.CAMP_CADASTRO_CPF}       ${CPF_USUARIO}

    Wait Until Element Is Visible           ${LOGIN.BTN_PROXIMO}
    Click Element                           ${LOGIN.BTN_PROXIMO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_NOME}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_NOME}      ${NOME_USUARIO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_DIA_NASC}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_DIA_NASC}       ${AUTH.DIA}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_MES_NASC}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_MES_NASC}       ${AUTH.MES}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_ANO_NASC}                
    Input Text                              ${LOGIN.CAMP_CADASTRO_ANO_NASC}       ${AUTH.ANO}

    Wait Until Element Is Visible           ${LOGIN.LABEL_SEXO}
    Click Element                           ${LOGIN.LABEL_SEXO}
    Click Element                           ${LOGIN.LABEL_SEXO}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_SENHA}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_SENHA}       ${AUTH.PASSWORD}

    Wait Until Element Is Visible           ${LOGIN.BTN_PROXIMO}
    Click Element                           ${LOGIN.BTN_PROXIMO}
    
    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_CEP}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_CEP}        ${AUTH.CEP}

    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_NUM}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_NUM}                ${AUTH.NUM}
    
    Wait Until Element Is Visible           ${LOGIN.CAMP_CADASTRO_COMPLEMENTO}  
    Input Text                              ${LOGIN.CAMP_CADASTRO_COMPLEMENTO}        ${AUTH.COMPLEMENTO}

    Wait Until Element Is Visible           ${LOGIN.BTN_CADASTRAR}
    Click Element                           ${LOGIN.BTN_CADASTRAR}

    Wait Until Element Is Visible           ${LOGIN.BTN_ACEITO}
    Click Element                           ${LOGIN.BTN_ACEITO}


E o login deve ser concluído com sucesso
    Wait Until Page Contains                ${LOGIN.LABEL_VALIDATION}

Então a mensagem de email invalido deve conter
    Wait Until Page Contains                 ${LOGIN.LABEL_VALIDATION_EMAIL}

Então a mensagem de telefone invalido deve conter
    Wait Until Page Contains                 ${LOGIN.LABEL_VALIDATION_PHONE}

Então a mensagem de login invalido deve conter
    Wait Until Page Contains                 ${LOGIN.LABEL_VALIDATION_NULL}

Então a mensagem de senha invalida deve conter
    Wait Until Page Contains                 ${LOGIN.LABEL_VALIDATION_PASSWORD}

Então a mensagem para validar a area deslogada deve conter
    Wait Until Page Contains                 ${LOGIN.LABEL_VALIDATION_VISITANTE}

Então a mensagem para validar o cadastro deve conter
    Wait Until Page Contains                 ${LOGIN.LABEL_VALIDATION_CADASTRO}