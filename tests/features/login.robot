*** Settings ***
Documentation           Como usuário do aplicativo DR Consulta, quero abrir o aplicativo e informando minhas credenciais
...                     devo realizar o login com sucesso.

Resource                ../../src/config/package.robot

Test Setup              Open Session ${CONFIG.MOBILE.ANDROID.PLATFORM_NAME} ${CONFIG.MOBILE.DEFAULT.AMBIENTE}
Test Teardown           Close Session

*** Test Cases ***
Cenario 1: Realizar login no aplicativo Dr Consulta com email
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de acesso de email
    E o login deve ser concluído com sucesso

Cenario 2: Realizar login no aplicativo Dr Consulta com telefone
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de acesso de telefone
    E o login deve ser concluído com sucesso

Cenario 3: Realizar login no aplicativo Dr Consulta passando email invalido
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de email invalido
    Então a mensagem de email invalido deve conter

Cenario 4: Realizar login no aplicativo Dr Consulta passando telefone invalido
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de telefone invalido
    Então a mensagem de telefone invalido deve conter

Cenario 5: Realizar login no aplicativo Dr Consulta passando senha invalida
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de senha invalida
    Então a mensagem de senha invalida deve conter

Cenario 6: Tentar realizar login no aplicativo Dr Consulta passando email null
    Dado que eu entre no aplicativo
    Quando não inserimos credenciais de email ou telefone
    Então a mensagem de login invalido deve conter
    
Cenario 7: Entrar no aplicativo Dr Consulta pela area nao logada
    Dado que eu abra o aplicativo
    Quando escolhemos navegar sem uma conta
    Então a mensagem para validar a area deslogada deve conter

Cenario 8: Criar cadastro de novo usuario com email
    Dado que eu entre no aplicativo
    Quando insiro dados novos para cadastro com email
    Então a mensagem para validar o cadastro deve conter

Cenario 9: Criar cadastro de novo usuario com celular
    Dado que eu entre no aplicativo
    Quando insiro dados novos para cadastro com celular
    Então a mensagem para validar o cadastro deve conter