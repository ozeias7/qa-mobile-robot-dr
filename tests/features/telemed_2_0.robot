*** Settings ***
Documentation           Como usuário do aplicativo DR Consulta, quero abrir o aplicativo e informando minhas credenciais
...                     devo realizar um atendimento de telemedicina 2.0.

Resource                ../../src/config/package.robot

Test Setup              Open Session ${CONFIG.MOBILE.ANDROID.PLATFORM_NAME} ${CONFIG.MOBILE.DEFAULT.AMBIENTE}
Test Teardown           Close Session

*** Test Cases ***
Cenario 1: Realizar consulta telemed 2.0 no app Dr Consulta pagando em 1x
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de acesso de email
    E o login deve ser concluído com sucesso
    Então devo realizar a consulta telemed 2.0 pagando em 1x

Cenario 2: Realizar consulta telemed 2.0 no app Dr Consulta pagando em 2x
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de acesso de email
    E o login deve ser concluído com sucesso
    Então devo realizar a consulta telemed 2.0 pagando em 2x

Cenario 3: Realizar consulta telemed 2.0 no app Dr Consulta pagando em 3x
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de acesso de email
    E o login deve ser concluído com sucesso
    Então devo realizar a consulta telemed 2.0 pagando em 3x
    
Cenario 4: Realizar a consulta telemed 2.0 pela pagina inicio
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de acesso de email
    E o login deve ser concluído com sucesso
    Então devo realizar a consulta telemed 2.0 pela pagina inicio

Cenario 5: Realizar a consulta telemed 2.0 pela pagina servicos
    Dado que eu entre no aplicativo
    Quando inserir as credenciais de acesso de email
    E o login deve ser concluído com sucesso
    Então devo realizar a consulta telemed 2.0 pela pagina servicos