*** Settings ***
Documentation           Setup responsável por importar todas as dependências do framework.
########################################################################
##                                                                    ##
##                    Libraries de todo o projeto                     ##
##                                                                    ##
## #####################################################################
## CRIADO POR: Diego Valenzuela 									  ##
## DATA: 19/05/2022													  ##
## ÁREA: QUALITY ASSURANCE                                            ##
## #####################################################################

Library                 AppiumLibrary
Library                 FakerLibrary        locale=pt_BR
Library                 DebugLibrary
Library                 OperatingSystem
Library                 Process
Library                 String
Library                 Collections
Library                 pabot.PabotLib

########################################################
#                    Keywords Mobile                   #
########################################################

Resource                ../auto/keywords/kws_login.robot
Resource                ../auto/keywords/kws_telemed_2_0.robot

########################################################
#                    Pages Mobile                      #
########################################################

Resource                ../auto/elements/android/elements_home.robot
Resource                ../auto/elements/android/elements_login.robot
Resource                ../auto/elements/android/elements_telemed_2_0.robot
Resource                ../auto/elements/android/elements_pagamentos.robot

########################################################
#                     Data Mobile                      #
########################################################
Variables               hooks.yaml
Variables               ../auto/data/login_data.yaml
Variables               ../auto/data/produtos.yaml
Variables               ../auto/data/pagamento_cartao_credito.yaml

########################################################
#                     Hooks /Utils                     #
########################################################
Resource                hooks.robot
Resource                ../utils/common.robot
Resource                ../utils/common_pagamentos.robot
